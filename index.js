const
    request     = require('request'),
    axios       = require('axios'),
    fs          = require('fs')
    ;

// const baseURL = 'https://c7c2ececa8bea44f97b2ee55b13997ea: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@coupleprintsateam.myshopify.com';
const baseURL = 'https://30bfba01af459d1808f4879926e950b9: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@podomize-com.myshopify.com';

function updateProductMetafield({productID, key, value, namespace='TAT'}) {
    const productMetafieldsURL = baseURL + `/admin/api/2019-04/products/${productID}/metafields.json`;
    axios.post(productMetafieldsURL, {
        metafield: {
            namespace: namespace,
            key: key,
            value: value,
            value_type: 'string'
        }
    })
    .catch(err => {
        console.log('Failed to set metafield');
        console.log(err);
        const logMessage = `\n${(new Date).toLocaleString()}\nFailed to set metafield:\n${JSON.stringify(err)}\n`;
        fs.appendFileSync('log.txt', logMessage);
    });
}

function readProductMetafields({productID}, callback) {
    if(!callback) return;
    const productMetafieldsURL = baseURL + `/admin/api/2019-04/products/${productID}/metafields.json`;
    axios.get(productMetafieldsURL)
    .then(response => {
        const metafields = response.data.metafields;
        callback(metafields);
    })
    .catch(err => {
        console.log(`Failed to read product metafields: ${productID}`);
        callback();
    })
}

function readProducts({lastID}={}, callback) {
    if(!callback) return;
    const productsDataURL = baseURL + `/admin/api/2019-04/products.json?limit=250&order=id` + (lastID ? `&last_id=${lastID}` : '');
    console.log(productsDataURL);
    axios.get(productsDataURL)
    .then(response => {
        const products = response.data.products;
        callback(products);
    })
    .catch(err => {
        console.log(`Failed to read products`);
    })
}

function main(lastID) {
    readProducts({lastID}, async products => {
        for(const product of products) {
            await new Promise(resolve => {
                readProductMetafields({ productID: product.id }, metafields => {
                    if(!metafields) return resolve();
                    const iframeURLMetafield = metafields.filter(metafield => metafield.key == 'iframe_url')[0];
                    const iframeURL = iframeURLMetafield?.value;
                    const replaced = 'https://dev.navitee.com/iframe/final?product=';
                    const replaceWith = 'https://d1c1ajca7f3paq.cloudfront.net/customize/index.html?product=';
                    const replaced2 = 'https://dev.navitee.com/iframe/final/?product=';
                    const replaceWith2 = 'https://d1c1ajca7f3paq.cloudfront.net/customize/index.html?product=';
                    const replaced3 = 'https://d1c1ajca7f3paq.cloudfront.net';
                    const replaceWith3 = 'https://d1vyizgcxsj7up.cloudfront.net';
                    const newIframeURL = iframeURL ? iframeURL.replace(replaced, replaceWith).replace(replaced2, replaceWith2).replace(replaced3, replaceWith3) : iframeURL;
                    const needUpdate = newIframeURL != iframeURL;
                    console.log({
                        productID: product.id,
                        iframeURL,
                        newIframeURL,
                        needUpdate
                    });
                    if(needUpdate) {
                        updateProductMetafield({
                            productID: product.id,
                            key: 'iframe_url',
                            value: newIframeURL,
                            namespace: iframeURLMetafield.namespace
                        });
                    }
                    setTimeout(() => {
                        resolve();
                    }, 1000);
                });
            })
        }
        if(products.length) {
            main(products.slice(-1)[0].id);
        }
    });
}
main('6705774657698');
